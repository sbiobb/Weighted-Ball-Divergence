\begin{theorem}
\label{theorem_asym}
We will always have
\begin{align*}
\mathcal{W}_{n,m}\xrightarrow[n,m\to\infty]{a.s.}\mathcal{W}(\mu,v),
\end{align*}
where $\mathcal{W}(\mu,v)$ has been defined in Section \ref{weighted_test}.
\end{theorem}

\begin{proof}[\textit{\textbf{Proof}}]
The basic idea of the proof is to separate the test statistics $\mathcal{W}_{n,m}$ into a U-statistics and a remainder. Let us start with $\mathcal{W}A_{n,m}$: 
\begin{align*}
\mathcal{W}A_{n,m}=&\frac{1}{n^2}\sum_{i,j=1}^{n}w(X_i,X_j)(A^X_{ij}-A^Y_{ij})^2\\
=&\frac{1}{n^2}\sum_{i,j=1}^{n}w(X_i,X_j)
\big\{\frac{1}{n}\sum_{u=1}^{n}\delta(X_{i},X_{j},X_{u})-\frac{1}{m}\sum_{v=1}^{m}\delta(X_{i},X_{j},Y_{v})\big\}^2\\
&\frac{1}{n^4m^2}\sum_{i,j,u,u^\prime=1}^{n}\sum_{v,v^\prime=1}^{m}w(X_i,X_j)\big\{\\
&\delta(X_i,X_j,X_u)\cdot\delta(X_i,X_j,X_{u^\prime})
+\delta(X_i,X_j,Y_v)\cdot\delta(X_i,X_j,Y_{v^\prime})\\
&-\delta(X_i,X_j,X_u)\cdot\delta(X_i,X_j,Y_v)
-\delta(X_i,X_j,X_{u^\prime})\cdot\delta(X_i,X_j,Y_{v^\prime})\big\}\\
=&\frac{4!2!}{n^4m^2}\sum_{i<j<u<u^\prime}^{n}\sum_{v<v^\prime}^{m}w(X_i,X_j)\big\{\\
&\delta(X_i,X_j,X_u)\cdot\delta(X_i,X_j,X_{u^\prime})
+\delta(X_i,X_j,Y_v)\cdot\delta(X_i,X_j,Y_{v^\prime})\\
&-\delta(X_i,X_j,X_u)\cdot\delta(X_i,X_j,Y_v)
-\delta(X_i,X_j,X_{u^\prime})\cdot\delta(X_i,X_j,Y_{v^\prime})\big\}+A_R\\
=&A_U+A_R,
\end{align*}
among which $A_R$ is the collection of the term in $\mathcal{W}A_{n,m}$ where at least one pair of $\{i,j,u,y^\prime\}$ or of $\{v,v^\prime\}$ share same values, and $A_U$ is the U-statistics extracted from $\mathcal{W}A_{n,m}$.

We first come to $A_U$, and our aim is to prove that it converges to $\mathcal{W}A$ as $n,m\to\infty$. Denote
\begin{align*}
&\psi_{A}(X_i,X_j,X_u,X_u^\prime; Y_v,Y_{v^\prime})\\
=&w(X_i,X_j)\Big\{\delta(X_i,X_j,X_u)\cdot\delta(X_i,X_j,X_{u^\prime})
+\delta(X_i,X_j,Y_v)\cdot\delta(X_i,X_j,Y_{v^\prime})\\
&-\delta(X_i,X_j,X_u)\cdot\delta(X_i,X_j,Y_v)
-\delta(X_i,X_j,X_{u^\prime})\cdot\delta(X_i,X_j,Y_{v^\prime})\Big\}.
\end{align*}
Therefore we can rewrite $A_U$ as
\begin{align*}
A_U=&\frac{4!2!}{n^4m^2}\sum_{i<j<u<u^\prime}^{n}\sum_{v<v^\prime}^{m}w(X_i,X_j)\big\{\\
&\delta(X_i,X_j,X_u)\cdot\delta(X_i,X_j,X_{u^\prime})
+\delta(X_i,X_j,Y_v)\cdot\delta(X_i,X_j,Y_{v^\prime})\\
&-\delta(X_i,X_j,X_u)\cdot\delta(X_i,X_j,Y_v)
-\delta(X_i,X_j,X_{u^\prime})\cdot\delta(X_i,X_j,Y_{v^\prime})\big\}\\
=&\frac{4!2!}{n^4m^2}\sum_{i<j<u<u^\prime}^{n}\sum_{v<v^\prime}^{m}\psi_{A}(X_i,X_j,X_u,X_u^\prime; Y_v,Y_{v^\prime})\\
=&\frac{(n-1)(n-2)(n-3)(m-1)}{n^3m(4!2!)}\big\{\frac{4!2!}{\binom{n}{4}\binom{m}{2}}\sum_{i<j<u<u^\prime}^{n}\sum_{v<v^\prime}^{m}\psi_{A}(X_i,X_j,X_u,X_u^\prime; Y_v,Y_{v^\prime})\big\}.
\end{align*}
It can be seen that although having a scale, $A_U$ is a U-statistics with kernel $\psi_{A}$ having degree 4 and 2. Moreover, since $w(u,v)$ is bounded and $\delta(x,y,z)\leq 1$ holds, we also have $E|\psi_{A}|<\infty$. Hence, according to Lemma \ref{lemma_U}, we will have
\begin{align*}
\lim_{n,m\to\infty}A_U=\big(&\lim_{n,m\to\infty}\frac{(n-1)(n-2)(n-3)(m-1)}{n^3m}\big)E\psi_{A}(X_i,X_j,X_u,X_u^\prime; Y_v,Y_{v^\prime})\\
=&E[E\big(\psi_{A}(X_1,X_2,X_3,X; Y_1,Y)|X_1,X_2\big)]\\
=&E\Big[w(X_1,X_2)\big[E\big(\delta(X_1,X_2,X)|X_1,X_2\big)-E\big(\delta(X_1,X_2,Y)|X_1,X_2\big)\big]^2\Big]\\
=&\mathcal{W}A.
\end{align*}

Since $A_U$ converges to $\mathcal{W}A$, now we should turn to $A_R$ and prove that it converges to $0$ as  $n,m\to\infty$. Actually, when at least one pair of $\{i,j,u,y^\prime\}$ or of $\{v,v^\prime\}$ share same values, the kernel $\psi_{A}$ will degenerate as a new one with lower degree. For instance, suppose $i=j$, $\psi_{A}(X_i,X_j,X_u,X_u^\prime; Y_v,Y_{v^\prime})$ will degenerate to $\psi^\prime_{A}(X_i,X_u,X_u^\prime; Y_v,Y_{v^\prime})$, which have degree 3 and 2. Because $E|\psi^\prime_{A}|<\infty$, in the situation where $i=j$, we can also use Lemma \ref{lemma_U} and have
\begin{align*}
&\lim_{n,m\to\infty}\big(\frac{1}{n^4m^2}\sum_{i=j<u<u^\prime}^{n}\sum_{v<v^\prime}^{m}\psi_{A}(X_i,X_j,X_u,X_u^\prime; Y_v,Y_{v^\prime})\big)\\
=&\lim_{n,m\to\infty}\big(\frac{1}{n^4m^2}\sum_{i<u<u^\prime}^{n}\sum_{v<v^\prime}^{m}\psi^\prime_{A}(X_i,X_u,X_u^\prime; Y_v,Y_{v^\prime})\big)\\
=&\lim_{n,m\to\infty}\big(\frac{1}{n}\cdot\frac{(n-1)(n-2)(m-1)}{n^2m}\cdot\frac{1}{3!2!}\big)\cdot \lim_{n,m\to\infty}\big\{\frac{1}{\binom{n}{3}\binom{m}{2}}\sum_{i<u<u^\prime}^{n}\sum_{v<v^\prime}^{m}\psi^\prime_{A}(X_i,X_u,X_u^\prime; Y_v,Y_{v^\prime})\big\}\\
=&\lim_{n,m\to\infty}\big(\frac{1}{n}\cdot\frac{(n-1)(n-2)(m-1)}{n^2m}\cdot\frac{1}{3!2!}\big)\cdot E\psi^\prime_{A}(X_i,X_u,X_u^\prime; Y_v,Y_{v^\prime})\\
=&0.
\end{align*}

Since $A_R$ only have finite number of elements, all of which converges to $0$ as $n,m\to\infty$, we can thus make the conclusion that
\begin{align*}
\lim_{n,m\to\infty}A_R=0.
\end{align*}

Now we have $\mathcal{W}A_{n,m}\xrightarrow[n,m\to\infty]{a.s.}\mathcal{W}A$.
Because $\mathcal{W}C_{n,m}$ and $\mathcal{W}C$ is totally symmetric of $\mathcal{W}A_{n,m}$ and $\mathcal{W}A$, similar conclusion can be easily made that $\mathcal{W}C_{n,m}\xrightarrow[n,m\to\infty]{a.s.}\mathcal{W}C$. Hence, with \begin{align*}
\mathcal{W}_{n,m}=\mathcal{W}A_{n,m}+\mathcal{W}C_{n,m} \quad and \quad\mathcal{W}=\mathcal{W}A+\mathcal{W}C, 
\end{align*}
we obtain that
\begin{align*}
\mathcal{W}_{n,m}\xrightarrow[n,m\to\infty]{a.s.}\mathcal{W},
\end{align*}
and thus complete the proof.
%Therefore, with this definition we can view $\mathcal{W}A_{n,m}$ as an V-statistics and denote its kernel by
%\begin{align*}
%&\psi_{A}(X_i,X_j,X_u,X_u^\prime; Y_v,Y_{v^\prime})\\
%=&w(X_i,X_j)\Big\{\delta(X_i,X_j,X_u)\cdot\delta(X_i,X_j,X_{u^\prime})
%+\delta(X_i,X_j,Y_v)\cdot\delta(X_i,X_j,Y_{v^\prime})\\
%&-\delta(X_i,X_j,X_u)\cdot\delta(X_i,X_j,Y_v)
%-\delta(X_i,X_j,X_{u^\prime})\cdot\delta(X_i,X_j,Y_{v^\prime})\Big\}.
%\end{align*}
%Likewise, denote the kernel of $\mathcal{W}C_{n,m}$ by
%\begin{align*}
%&\psi_{C}(X_u,X_{u^\prime}; Y_k,Y_l,Y_v,Y_{v^\prime})\\
%=&w(Y_k,Y_l)\Big\{\delta(Y_k,Y_l,Y_v)\cdot\delta(Y_k,Y_l,Y_{v^\prime})
%+\delta(Y_k,Y_l,X_u)\cdot\delta(Y_k,Y_l,X_{u^\prime})\\
%&-\delta(Y_k,Y_l,Y_v)\cdot\delta(Y_k,Y_l,X_u)
%-\delta(Y_k,Y_l,Y_v^{\prime})\cdot\delta(Y_k,Y_l,X_{u^\prime})\Big\}.
%\end{align*}
%
%Then dealing with the expectation of $\psi_{A}(X_i,X_j,X_u,X_u^\prime; Y_v,Y_{v^\prime})$ by conditional expectation properties, it will have
%\begin{align*}
%&E\psi_{A}(X_1,X_2,X_3,X; Y_1,Y)\\
%=&E[E\big(\psi_{A}(X_1,X_2,X_3,X; Y_1,Y)|X_1,X_2\big)]\\
%=&E\Big[w(X_1,X_2)\big[E\big(\delta(X_1,X_2,X)|X_1,X_2\big)-E\big(\delta(X_1,X_2,Y)|X_1,X_2\big)\big]^2\Big]\\
%=&\mathcal{W}A,
%\end{align*}
%and
%\begin{align*}
%&E\psi_{C}(X_1,X; Y_1,Y_2,Y_3,Y)\\
%=&E[E\big(\psi_{C}(X_1,X; Y_1,Y_2,Y_3,Y)|Y_1,Y_2\big)]\\
%=&E\Big[w(X_1,X_2)\big[\delta(Y_1,Y_2,X)|Y_1,Y_2\big)-E\big(\delta(Y_1,Y_2,Y)|Y_1,Y_2\big)\big]^2\Big]\\
%=&\mathcal{W}C.
%\end{align*}
%
%According to Lee's \cite{Lee_1990} asymptotic theory, with the above equations and $E|\psi_{A}|, E|\psi_{C}|<\infty$, we can have both $\mathcal{W}A_{n,m}$ and $\mathcal{W}C_{n,m}$ converge a.s. to $\mathcal{W}A$ and $\mathcal{W}C$ respectively. Thus we finish the proof.
\end{proof}
