Weighted Ball Divergence (WBD) is a generalization of BD. Since the BD measures the differences between $\mu$ and $\nu$ by comparing them in the perspective of balls, weights can be added to these comparisons. The definition of Weighted Ball Divergence is:
\begin{align*}
\mathcal{W}(\mu,\nu)=\iint_{V}w(u,v)[\mu-\nu]^2\big(
\bar{B}(u,d(u,v))\big)(\mu(du)\mu(dv)+\nu(du)\nu(dv)),
\end{align*}
where weights $w(u,v)$ is a positive bivariate function and $w(u, v) < \infty$. If $w(u, v) \equiv 1$, WBD is equivalent to BD. For WBD, we provide a fundamental theorem which play a role as keystone in the WBD-based two sample test procedure.

\begin{theorem}
\label{theorem_positive}
Suppose $\mu$ and $\nu$ are two Borel probability measures on a finite dimensional Banach space V, then $\mathcal{W}(\mu,\nu)\ge 0$ holds and equality will be reached if and only if $\mu=\nu$.
\end{theorem}

\begin{proof}[\textit{\textbf{Proof}}]
As $w(u,v) > 0$, we have $\mathcal{W}(\mu,\nu) \geq 0$ always holds. On the other hand, when $\mu=\nu$, it also can be easily seen that $\mathcal{W}(\mu,\nu)=0$. Therefore, the key point is that whether $\mathcal{W}(\mu,\nu)=0$ always means $\mu=\nu$, in other word, $\mu(B)=\nu(B)$ for $B \in \mathcal{B}_{0}$.

% Since $w(u,v)>0$, $\mathcal{W}(\mu,\nu)=0$ implies that there exists $\mu$-null set $V_0$, such that $\mu=\nu$ on $\bar{B}(u,\rho(u,v))$ for $u,v\in V/V_0$. 

Let $S_{\mu}$ be the support of $\mu$ consisting of the points such that every of their open neighborhoods has positive measure, and
$S_{\nu}$ the support of $\nu$. Then $S^c_{\mu}=\mathrm{V}/S_{\mu}$ is the union of all $\mu$-null open sets. Also, since $\mathrm{V}$ is separable, $\mu(S^c_{\mu})=0$. $\mathcal{W}(\mu,\nu)=0$ implies that
\begin{align*}
\iint_{\mathrm{S_\mu}\times \mathrm{S_\mu}} w(u, v)[\mu-\nu]^2
(\bar{B}(u,\rho(u,v)))\mu(du) \mu(dv)=0.
\end{align*}
Since $[\mu-\nu]^2 (\bar{B}(u,\rho(u,v)))$ is nonnegative and $w(u, v) > 0$, we have
\begin{align*}
[\mu-\nu]^2 (\bar{B}(u,\rho(u,v)))=0.\quad a.s.
\end{align*}
This condition can ensure $\mu(B)=\nu(B)$ for $B \in \mathcal{B}_{0}$ according to Theorem 1 in Pan \cite{pan2017ball}. Consequently, we are able to reach the conclusion that $\mathcal{W}(\mu,\nu)=0$ implies $\mu=\nu$, and complete the proof.
\end{proof}

For convenience, we decompose $\mathcal{W}(\mu,\nu)$ into $\mathcal{W}A$ and $\mathcal{W}C$, where
\begin{align*}
\mathcal{W}A&=\iint_{V}w(u,v)[\mu-\nu]^2\big(
\bar{B}(u,d(u,v))\big)\mu(du)\mu(dv),
\end{align*}
and
\begin{align*}
\mathcal{W}C&=\iint_{V}w(u,v)[\mu-\nu]^2\big(
\bar{B}(u,d(u,v))\big)\nu(du)\nu(dv).
\end{align*}

Sample versions of $\mathcal{W}A, \mathcal{W}C$ are defined following as:
\begin{align*}
&\mathcal{W}A_{n,m}=\frac{1}{n^2}\sum_{i,j=1}^{n}\hat{w}_{n,m}(X_i,X_j)(A^X_{ij}-A^Y_{ij})^2,\\
&\mathcal{W}C_{n,m}=\frac{1}{m^2}\sum_{k,l=1}^{m}\hat{w}_{n,m}(Y_k,Y_l)(C^X_{kl}-C^Y_{kl})^2,
\end{align*}
where $A^X_{ij},A^Y_{ij},C^X_{kl}$ and $C^Y_{kl}$ share the same definitions as in Section \ref{multivariate_test}, and $\hat{w}_{n,m}(X_i,X_j)$ is the sample estimation of $w(u,v)$. We can finally define our sample version of Weight Ball Divergence statistics by
\begin{align*}
\mathcal{W}_{n,m}=\mathcal{W}A_{n,m}+\mathcal{W}C_{n,m}.
\end{align*}
%The null hypothesis of the weighted test is $H_0: \mu=\nu$ while the alternative hypothesis is $H_1: \mu\neq\nu$. And the test rejects null hypothesis for large value of $\mathcal{W}_{n,m}$.
